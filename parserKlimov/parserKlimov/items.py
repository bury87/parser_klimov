# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class LamodaItem(scrapy.Item):
    # define the fields for your item here like:
    url = scrapy.Field()
    cat = scrapy.Field()
    img = scrapy.Field()
    title = scrapy.Field()
    price = scrapy.Field()
    price_discount = scrapy.Field()
    currency = scrapy.Field()
    brand = scrapy.Field()
    pass

class TrendsbrandsItem(scrapy.Item):
    # define the fields for your item here like:
    url = scrapy.Field()
    cat = scrapy.Field()
    img = scrapy.Field()
    title = scrapy.Field()
    price = scrapy.Field()
    price_discount = scrapy.Field()
    currency = scrapy.Field()
    brand = scrapy.Field()
    description = scrapy.Field()
    pass

class RespublicaItem(scrapy.Item):
    # define the fields for your item here like:
    url = scrapy.Field()
    cat = scrapy.Field()
    img = scrapy.Field()
    title = scrapy.Field()
    price = scrapy.Field()
    price_discount = scrapy.Field()
    currency = scrapy.Field()
    pass

class OzonItem(scrapy.Item):
    # define the fields for your item here like:
    url = scrapy.Field()
    cat = scrapy.Field()
    img = scrapy.Field()
    title = scrapy.Field()
    price = scrapy.Field()
    price_discount = scrapy.Field()
    currency = scrapy.Field()
    pass

class KomusItem(scrapy.Item):
    # define the fields for your item here like:
    url = scrapy.Field()
    cat = scrapy.Field()
    img = scrapy.Field()
    brand = scrapy.Field()
    title = scrapy.Field()
    price = scrapy.Field()
    price_discount = scrapy.Field()
    currency = scrapy.Field()
    pass

class HoffItem(scrapy.Item):
    # define the fields for your item here like:
    brand = scrapy.Field()
    url = scrapy.Field()
    cat = scrapy.Field()
    img = scrapy.Field()
    title = scrapy.Field()
    price = scrapy.Field()
    price_discount = scrapy.Field()
    currency = scrapy.Field()
    pass

class BoscoItem(scrapy.Item):
    # define the fields for your item here like:
    brand = scrapy.Field()
    url = scrapy.Field()
    cat = scrapy.Field()
    img = scrapy.Field()
    title = scrapy.Field()
    price = scrapy.Field()
    price_discount = scrapy.Field()
    currency = scrapy.Field()
    pass

class WildberryItem(scrapy.Item):
    # define the fields for your item here like:
    url = scrapy.Field()
    brand = scrapy.Field()
    cat = scrapy.Field()
    img = scrapy.Field()
    title = scrapy.Field()
    price = scrapy.Field()
    price_discount = scrapy.Field()
    currency = scrapy.Field()
    pass

class TsumItem(scrapy.Item):
    # define the fields for your item here like:
    url = scrapy.Field()
    brand = scrapy.Field()
    cat = scrapy.Field()
    img = scrapy.Field()
    title = scrapy.Field()
    price = scrapy.Field()
    price_discount = scrapy.Field()
    currency = scrapy.Field()
    pass

class VarvaraItem(scrapy.Item):
    # define the fields for your item here like:
    url = scrapy.Field()
    brand = scrapy.Field()
    cat = scrapy.Field()
    img = scrapy.Field()
    title = scrapy.Field()
    price = scrapy.Field()
    description = scrapy.Field()
    price_discount = scrapy.Field()
    currency = scrapy.Field()
    pass

class DressoneItem(scrapy.Item):
    # define the fields for your item here like:
    url = scrapy.Field()
    brand = scrapy.Field()
    cat = scrapy.Field()
    img = scrapy.Field()
    title = scrapy.Field()
    price = scrapy.Field()
    description = scrapy.Field()
    price_discount = scrapy.Field()
    currency = scrapy.Field()
    pass

class KalashnikovyItem(scrapy.Item):
    # define the fields for your item here like:
    url = scrapy.Field()
    brand = scrapy.Field()
    cat = scrapy.Field()
    img = scrapy.Field()
    title = scrapy.Field()
    price = scrapy.Field()

    price_discount = scrapy.Field()
    currency = scrapy.Field()
    pass

class RoomchikItem(scrapy.Item):
    # define the fields for your item here like:
    url = scrapy.Field()
    brand = scrapy.Field()
    cat = scrapy.Field()
    img = scrapy.Field()
    title = scrapy.Field()
    price = scrapy.Field()

    price_discount = scrapy.Field()
    currency = scrapy.Field()
    pass
# coding: utf8
import scrapy
from scrapy.http import FormRequest
import re
import json
import csv
import sys

from parserKlimov.items import OzonItem

class LamodaSpider(scrapy.Spider):
    name = "ozon"
    # handle_httpstatus_list = [429,401]
    pipeline = 'ozon'
    shop = {
		"url" : u"http://www.ozon.ru/",
		"title" : u"Купить мужскую одежду и обувь в интернет магазине Lamoda.ru",
		"logo" : u"//ozon-st.cdn.ngenix.net/graphics/ozon/170210-logo_love.png"
	}


    def start_requests(self):
        urls = ['http://www.ozon.ru/']
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)


    def parse(self, response):
        urls = response.xpath(".//ul[@class='eMainMenu_TopLevelList']/li[contains(@class,'mCommonItem')]/a/@href").extract()
        for url in urls:
            yield scrapy.Request(url=response.urljoin(url), callback=self.parse1)


    def parse1(self, response):
        urls = response.xpath(".//div[@class='eLeftMainMenu_ElementsBlock']/a[@class='eLeftMainMenu_Link ']/@href").extract()

        for url in urls:
            yield scrapy.Request(url=response.urljoin(url), callback=self.parse2)

    def parseAuth(self,response):
        jsonData = json.loads(response.body)
        print jsonData

    def parse2(self, response):
        count_items = response.xpath(".//div[@class='bTilesModeShow clear_group']/div[contains(@class,'bOneTile')]/a[contains(@class,'eOneTile_tileLink')]/@href").extract_first(default=None)
        if count_items is not None:

            count_items = int(re.sub('\D','',count_items))
            facetParams = response.url.split('?')

            yield scrapy.Request(url='http://www.ozon.ru/account/authorize.aspx', callback=self.parseAuth)
            for offset in range(0,count_items,100):

                post_param = {'context': "catalog", 'facetId': '62', 'facetParams':facetParams, 'limit': '21', 'offset':str(offset),'searchText':""}
                url = 'http://www.ozon.ru/json/tiles.asmx/gettiles'
                yield FormRequest(url=url,formdata=post_param, callback=self.parseJson)
        else:
            urls = response.xpath(".//div[@id='facetControl_catalog']/div/div/ul[contains(@class,'eFilterList_List')]/li/a/@href").extract()
            for url in urls:
                yield scrapy.Request(url=response.urljoin(url), callback=self.parse2)

    def parseJson(self,response):
        jsonData = json.loads(response.body)

        for url in jsonData['d']['Tiles']:
            yield scrapy.Request(url=response.urljoin(url['Href']), callback=self.parse4)


    def parse4(self, response):
        product = LamodaItem()
        product['url'] = response.url
        product['cat'] = response.xpath(".//div[contains(@class,'bBreadCrumbs')]/div/a/span/text()").extract()
        # print ','.join(product['cat']).encode('cp1251')

        images =  response.xpath(".//div[contains(@class,'eMicroGallery_previewsOne')]/img/@src").extract()
        product['img'] = map(self.ing_replace,images)
        product['title'] = response.xpath(".//h1[contains(@class,'bItemName')]/text()").extract_first()
        product['price'] = response.xpath(".//div[@class='eSaleBlock_colorWrap']/div[@class='bOzonPrice']/span[contains(@class,'eOzonPrice_main')]/text()").extract_first(default=None).replace(' ','')
        product['price_discount'] = ''
        product['currency'] ='RUB'
        yield product

    def ing_replace(self,img):
        return img.replace('\\c50','')


















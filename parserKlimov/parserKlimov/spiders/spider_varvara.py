# coding: utf8
import scrapy
import re
import json
import csv
import sys

from parserKlimov.items import VarvaraItem

class WildberrySpider(scrapy.Spider):
    name = "varvara"
    pipeline = 'varvara'
    shop = {
		"url" : u"http://varvara-shop.com/",
		"title" : u"Интернет-магазин дизайнерской одежды в Москве | Дизайнерская женская одежда | Дизайнерская одежда - купить",
		"logo" : u"https://assets.kiiiosk.ru/uploads/shop/131/favicons/cb5e8d1a-e4b9-4adb-9cab-6e48eb371ed8.jpg"
	}


    def start_requests(self):
        urls = ['http://varvara-shop.com/']
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)


    def parse(self, response):
        urls = response.xpath(".//li[position()>1 and position()<9]/a[@class='b-nav__link']/@href").extract()
        for url in urls:
            yield scrapy.Request(url=response.urljoin(url), callback=self.parse1)


    def parse1(self, response):
        urls = response.xpath(".//li[contains(@class,'maincatalog-menu-item') and position()>1]/a/@href").extract()
        for url in urls:
                yield scrapy.Request(url=response.urljoin(url), callback=self.parse2)

    def parse2(self, response):
        urls = response.xpath(".//li[@class='selected hasnochild']/ul/li/a/@href").extract()
        for url in urls:
                yield scrapy.Request(url=response.urljoin(url), callback=self.parse3)


    def parse3(self, response):
        urls = response.xpath(".//a[@data-type='perehod_na_kartochku_tovara' and contains(@class,'catalog-list-item-title')]/@href").extract()
        cat = response.xpath(".//div[@class='breadcrumbs']/a/text()").extract().append(response.xpath(".//div[@class='breadcrumbs']/span/text()").extract_first())
        for url in urls:
            yield scrapy.Request(url=response.urljoin(url), callback=self.parse4, meta={'cat':cat})
        next_page = response.xpath(".//a[contains(@class,'pagination-by-step-right')]/@href").extract_first(
            default=None)
        if next_page is not None:
            yield scrapy.Request(url=response.urljoin(next_page), callback=self.parse3)

    def parse4(self, response):
        product = WildberryItem()
        product['url'] = response.url
        product['cat'] = response.meta['cat']
        # print ','.join(product['cat']).encode('cp1251')
        product['img'] =  response.xpath(".//ul[@class='carousel']/li/a/@href").extract()
        product['title'] = response.xpath(".//h1[@itemprop='name']/text()").extract_first().strip()
        product['price'] = response.xpath(".//meta[@itemprop='price']/@content").extract_first(default='')
        product['brand'] = response.xpath(".//a[@id='brandBannerImgRef']/@title").extract_first(default='')
        product['price_discount'] = response.xpath(".//span/del/text()").extract_first(default='').replace(u'руб.','').replace(u' ','').replace(u',','.').replace(u'\xa0', u'').replace(u'\n', u'')
        product['currency'] ='RUB'
        yield product


















# coding: utf8
import scrapy
import re
import json
import csv
import sys

from parserKlimov.items import DressoneItem

class DressoneSpider(scrapy.Spider):
    name = "dressone"
    pipeline = 'dressone'
    shop = {
		"url" : u"http://www.dressone.ru/",
		"title" : u"Dressone.ru - интернет магазин женской одежды и аксессуаров от лучших российских дизайнеров. Информация о доставке и оплате.",
		"logo" : u"//www.dressone.ru.images.1c-bitrix-cdn.ru/bitrix/templates/dressUp/img/src/main_logo.png?13718188187066"
	}


    def start_requests(self):
        urls = ['http://www.dressone.ru/']
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)


    def parse(self, response):
        urls = response.xpath(".//li[@class='nav__item _hasChild js-has-drop']/a/@href").extract()
        for url in urls:
            if url not in ['/designers/','#']:
                yield scrapy.Request(url=response.urljoin(url), callback=self.parse1)


    def parse1(self, response):
        pages = response.xpath(".//div[@class='md_pages fleft']/ul/li[last()]/a/text()").extract_first()
        for page in range(1,int(pages)):
            yield scrapy.Request(url=response.url+'?PAGEN_1='+str(page), callback=self.parse2)


    def parse2(self, response):
        urls = response.xpath(".//div[@class='md_margin_1']/a/@href").extract()
        for url in urls:
                yield scrapy.Request(url=response.urljoin(url), callback=self.parse4)


    def parse4(self, response):
        product = DressoneItem()
        product['url'] = response.url
        product['cat'] = response.xpath(".//section[@class='md_pagecrossing']/ul/li/a/text()").extract()
        # print ','.join(product['cat']).encode('cp1251')
        product['img'] =  response.xpath(".//a[contains(@class,'product-gallery__thumb')]/@data-big").extract()
        product['title'] = response.xpath(".//span[contains(@class,'md_pf_b_4')]/text()").extract_first().strip()
        product['price'] = self.clear_price(response.xpath(".//span[contains(@class,'md_pf_b_7')]/text()").extract_first(default=''))
        product['brand'] = response.xpath(".//span[contains(@class,'md_pf_b_3')]/text()").extract_first(default='')
        product['price_discount'] = self.clear_price(response.xpath(".//span[contains(@class,'md_pf_b_8')]/text()").extract_first(default=''))
        product['description'] = response.xpath(".//p[@itemprop='description']/text()").extract_first(default='')
        product['currency'] ='RUB'
        yield product

    def clear_price(self, str):
        pattern = re.compile(r'(\s|\(.+\)|₽|\D|\.|-)')
        return pattern.sub(u'', str)
















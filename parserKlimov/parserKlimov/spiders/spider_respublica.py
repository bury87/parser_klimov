# coding: utf8
import scrapy
import re
import json
import csv
import sys

from parserKlimov.items import RespublicaItem

class LamodaSpider(scrapy.Spider):
    name = "respublica"
    pipeline = 'respublica'
    shop = {
		"url" : u"http://www.respublica.ru",
		"title" : u"Интернет-магазин Республика",
		"logo" : u"http://www.respublica.ru/assets/image-respublica-logo-59f57e7c4d01deace190584636cbef0a.png"
	}


    def start_requests(self):
        urls = ['http://www.respublica.ru/']
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)


    def parse(self, response):
        urls = response.xpath(".//div[@class='top-categories']/div/ul/li[position()<11]/a/@href").extract()
        for url in urls:
            yield scrapy.Request(url=response.urljoin(url), callback=self.parse1,)


    def parse1(self, response):
        urls = response.xpath(".//div[@class='category-items cfix']/div/div/div/a/@href").extract()
        for url in urls:
            yield scrapy.Request(url=response.urljoin(url), callback=self.parse2)
        print response.body
        pages = response.xpath(".//div[@class='pagination']/div/ul/li[last()]/a[contains(@class,'js-link js-items-update-action')]/text()").extract_first(default=None)
        print '**********************************'
        print pages
        if pages is not None:
            for page in range(2, int(pages)):
                url = response.url+'?order=new&page='+str(page)
                print url
                yield scrapy.Request(url=url, callback=self.parseRe)

    def parseRe(self, response):
        print response.body
        item_patern = re.compile(r"\$\('\.items-container'\)\.append\(\"(.+).+\);")
        items_html = item_patern.search(response.body).group(1).replace("\\",'')
        print items_html


    def parse2(self, response):
        product = RespublicaItem()
        product['url'] = response.url
        product['cat'] = response.xpath(".//div[@class='category-bc']/div/div/span/a/span/text()").extract()
        # print ','.join(product['cat']).encode('cp1251')
        product['img'] = response.xpath(".//div[@class='slides']/ul/li[@class != 'cloned']/div/img/@src").extract()
        product['title'] = response.xpath(".//h1[@itemprop='name']/text()").extract_first()

        product['price'] = response.xpath(".//div[@class='item-page-price']/meta[@itemprop='price']/@content").extract_first(
            default='')
        product['price_discount'] = response.xpath(
                ".//div[@class='item-page-price-sale price-sale']/text()").extract_first(default='').replace(' ','')
        product['currency'] = 'RUB'
        yield product




















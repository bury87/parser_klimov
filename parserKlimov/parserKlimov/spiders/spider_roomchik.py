# coding: utf8
import scrapy
import re
import json
import csv
import sys

from parserKlimov.items import RoomchikItem

class RoomchikSpider(scrapy.Spider):
    name = "roomchik"
    pipeline = 'roomchik'
    shop = {
		"url" : u"https://www.roomchik.ru/",
		"title" : u"Интернет-магазин \"Roomchik\"",
		"logo" : u"http://roomchik.ru/img/logo.gif"
	}


    def start_requests(self):
        urls = ['https://www.roomchik.ru/']
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)


    def parse(self, response):
        urls = response.xpath(".//div[@class='drop-holder']/ul/li/a/@href").extract()
        for url in urls:
            yield scrapy.Request(url=response.urljoin(url)+'?SHOWALL_2=1', callback=self.parse1)


    def parse1(self, response):
        urls = response.xpath(".//div[@class='goods-hover']/a/@href").extract()
        for url in urls:
                yield scrapy.Request(url=response.urljoin(url), callback=self.parse4)


    def parse4(self, response):
        product = RoomchikItem()
        product['url'] = response.url
        product['cat'] = response.xpath(".//div[@class='breadcrumbs']/ul/li/a/text()").extract()
        # print ','.join(product['cat']).encode('cp1251')
        product['img'] =  self.change_ing(response.xpath(".//ul[@class='pagination clearfix']/li/a/img/@src").extract())
        product['title'] = response.xpath(".//h2[@class='g-title']/text()").extract_first().strip()
        product['price'] = self.clear_price(response.xpath(".//span[contains(@class,'price ')]/text()").extract_first(default=''))
        product['brand'] = ''
        product['price_discount'] = self.clear_price(response.xpath(".//div[@class='g-many']/span[contains(@class,'del')]/text()").extract_first(default=''))
        product['currency'] ='RUB'
        yield product



    def change_ing(self, lst):
        nlist = []
        if len(lst)>0:
            for l in lst:
                nlist.append(l.replace('/resize_cache','').replace('90_130_2/',''))
        return nlist

    def clear_price(self, str):
        pattern = re.compile(r'(\s|\(.+\)|₽|\D|\.|-)')
        return pattern.sub(u'', str)














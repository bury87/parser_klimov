# coding: utf8
import scrapy
import re
import json
import csv
import sys

from parserKlimov.items import KalashnikovyItem

class KalashnikovySpider(scrapy.Spider):
    name = "kalashnikovy"
    pipeline = 'kalashnikovy'
    shop = {
		"url" : u"https://kalashnikovy.ru/",
		"title" : u"Купить дизайнерскую одежду в Москве: интернет-магазин модной женской одежды Anastasia&Olga Kalashnikovy",
		"logo" : u"https://kalashnikovy.ru/wp-content/uploads/2016/08/oEW3bTRFloY.jpg"
	}


    def start_requests(self):
        urls = ['https://kalashnikovy.ru/magazin-dizajnerskoj-odezhdy/?allview']
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)


    def parse(self, response):
        urls = response.xpath(".//a[@class='thumb-link']/@href").extract()
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse4)




    def parse4(self, response):
        product = KalashnikovyItem()
        product['url'] = response.url
        product['cat'] = response.xpath(".//nav/a[position()<3]/text()").extract()
        # print ','.join(product['cat']).encode('cp1251')
        product['img'] =  response.xpath(".//a[@data-rel='zoomImage']/@href").extract()
        product['title'] = response.xpath(".//h1[@itemprop='name']/text()").extract_first().strip()
        product['price'] = response.xpath(".//meta[@itemprop='price']/@content").extract_first(default='')
        product['brand'] = u'Kalashnikovy'
        product['price_discount'] = self.clear_price(response.xpath(".//p/del/span[contains(@class,'woocommerce-Price-amount amount')]/span[@class='rus']/text()").extract_first(default=''))
        product['currency'] ='RUB'

        yield product

    def clear_price(self, str):
        pattern = re.compile(r'(\s|\(.+\)|₽|\D|\.|-)')
        return pattern.sub(u'', str)

















# coding: utf8
import scrapy
import re
import json
import csv
import sys

from parserKlimov.items import TsumItem

class TsumSpider(scrapy.Spider):
    name = "tsum"
    pipeline = 'tsum'
    shop = {
		"url" : u"https://www.tsum.ru/",
		"title" : u"ЦУМ - интернет-магазин одежды, обуви и аксессуаров ведущих мировых брендов",
		"logo" : u"https://www.tsum.ru/local/gulp/dist/assets/images/logo.svg"
	}


    def start_requests(self):
        urls = ['https://www.tsum.ru/']
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)


    def parse(self, response):
        urls = response.xpath(".//li[@class='header__item' and position()>3]/a/@href").extract()
        for url in urls:
            yield scrapy.Request(url=response.urljoin(url), callback=self.parse3)


    def parse1(self, response):
        urls = response.xpath(".//li[contains(@class,'maincatalog-menu-item') and position()>1]/a/@href").extract()
        for url in urls:
                yield scrapy.Request(url=response.urljoin(url), callback=self.parse3)

    def parse2(self, response):
        urls = response.xpath(".//li[@class='selected hasnochild']/ul/li/a/@href").extract()
        for url in urls:
                yield scrapy.Request(url=response.urljoin(url), callback=self.parse3)

    def parse3(self, response):
        items = self.clear_price(response.xpath(".//div[contains(@class,'product-list__count js-products-count')]/text()").extract_first(default=''))
        if items is not '':
            pages = round(int(items)/30)
            for page in range(1,int(pages)):
                yield scrapy.Request(url=response.url+'?page='+str(page), callback=self.parse4)


    def parse4(self, response):
        urls = response.xpath(".//div[contains(@class,'product product_type_catalog')]/a/@href").extract()
        for url in urls:
            yield scrapy.Request(url=response.urljoin(url), callback=self.parse5)

    def parse5(self, response):
        product = TsumItem()
        product['url'] = response.url
        product['cat'] = response.xpath(".//li[@class='breadcrumbs__item']/a/text()").extract()
        # print ','.join(product['cat']).encode('cp1251')
        product['img'] =  response.xpath(".//img[contains(@class, 'slider-item__image')]/@src").extract()
        product['title'] = response.xpath(".//span[contains(@class, 'item__description')]/text()").extract_first().strip()
        product['price'] = self.clear_price(response.xpath(".//div[contains(@class, 'price price_type_old js-price')]/text()").extract_first(default=''))
        product['brand'] = response.xpath(".//h1[contains(@class, 'item__name')]/a/text()").extract_first(default='').strip()
        product['price_discount'] = self.clear_price(response.xpath(".//div[contains(@class, 'price price_type_new')]/text()").extract_first(default=''))
        if product['price_discount'] == '':
            product['price'] = self.clear_price(
                response.xpath(".//div[contains(@class, 'price js-price')]/text()").extract_first(
                    default=''))
        product['currency'] ='RUB'
        yield product

    def clear_price(self,str):
        pattern = re.compile(r'(\s|\(.+\)|₽|\D)')
        return pattern.sub(u'',str)


















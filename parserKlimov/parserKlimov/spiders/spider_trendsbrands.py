# coding: utf8
import scrapy
import re
import json
import csv
import sys

from parserKlimov.items import TrendsbrandsItem

class TrendsbrandsSpider(scrapy.Spider):
    name = "trendsbrands"
    pipeline = 'trendsbrands'
    shop = {
		"url" : u"http://www.trendsbrands.ru",
		"title" : u"Интернет-магазин одежды – большой и удобный каталог модной одежды для женщин на сайте TrendsBrands.ru",
		"logo" : u"http://www.trendsbrands.ru/img/logo/logo-14feb_2016.gif"
	}


    def start_requests(self):
        urls = ['http://www.trendsbrands.ru/catalog/woman/new/']
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)


    def parse(self, response):
        urls = response.xpath(".//div[@class='products']/div/a/@href").extract()
        for url in urls:
            yield scrapy.Request(url=response.urljoin(url), callback=self.parse1)
        pages = response.xpath(".//ul[@class='pageNavigation']/li[last()]/a/text()").extract_first()
        for page in range(2, int(pages)):
            url = response.url+'?PAGEN_1='+str(page)
            yield scrapy.Request(url=url, callback=self.parse1pag)

    def parse1pag(self, response):
        urls = response.xpath(".//div[@class='products']/div/a/@href").extract()
        for url in urls:
            yield scrapy.Request(url=response.urljoin(url), callback=self.parse1)


    def parse1(self, response):
        product = TrendsbrandsItem()
        product['url'] = response.url
        product['cat'] = response.xpath(".//div[@class='breadcrumbs']/a/span/text()").extract()
        product['img'] = response.xpath(".//div[@class='previews']/img/@data-image-previews-large-url").extract()
        product['title'] = response.xpath(
            ".//h1[@class='itemTitle title']/span[@itemprop='name']/text()").extract()
        product['brand'] = response.xpath(".//span[@itemprop='brand']/a/text()").extract()
        product['description'] = response.xpath(".//span[@itemprop='description']/text()").extract_first(
            default='')
        product['price'] = response.xpath(".//div[contains(@class,'blockItemDescription')]/@data-widget-product-price").extract_first(
            default='')
        product['price_discount'] = self.clear_price(response.xpath(
                ".//div[contains(@class,'blockItemDescription')]/div/div/span/text()").extract_first(default='').replace(' ','').replace(u'руб.',''))
        product['currency'] = 'RUB'
        yield product

    def clear_price(self, str):
        pattern = re.compile(r'(\s|\(.+\)|₽|\D|\.|-)')
        return pattern.sub(u'', str)



















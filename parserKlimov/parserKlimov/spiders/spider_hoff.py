# coding: utf8
import scrapy
import re
import json
import csv
import sys

from parserKlimov.items import HoffItem

class HoffSpider(scrapy.Spider):
    name = "hoff"
    pipeline = 'hoff'
    shop = {
		"url" : u"http://www.hoff.ru/",
		"title" : u"Hoff: европейский гипермаркет мебели и товаров для дома, интернет-магазин мебели",
		"logo" : u"https://hoff.ru/local/templates/responsive/images/logo-big.png"
	}


    def start_requests(self):
        urls = ['https://hoff.ru/']
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)


    def parse(self, response):
        urls = response.xpath(".//ul[@class='nav navbar-nav']/li/a/@href").extract()
        print urls
        for url in urls:
            yield scrapy.Request(url=response.urljoin(url+'?page=all'), callback=self.parse1)


    def parse1(self, response):
        urls = response.xpath(".//span[@class='item-image ']/a/@href").extract()
        for url in urls:
            yield scrapy.Request(url=response.urljoin(url), callback=self.parse2)
        next_page = response.xpath(".//ul[@class='list-pagination']/li[@class='next']/a/@href").extract_first(default=None)
        if next_page is not None:
            yield scrapy.Request(url=response.urljoin(next_page), callback=self.parse1)





    def parse2(self, response):
        product = HoffItem()
        product['url'] = response.url
        product['cat'] = response.xpath(".//nav[contains(@class,'product-breadcrumbs')]/ul/li/a/text()").extract()
        # print ','.join(product['cat']).encode('cp1251')
        product['img'] =  response.xpath(".//div[contains(@id,'prodSlider')]//img/@src").extract()
        product['title'] = response.xpath(".//h1[contains(@class,'product-main-head')]/text()").extract_first()
        product['brand'] = ''
        product['price'] = self.clear_price(response.xpath(".//*[contains(@class,'product-new-price')]/@data-price").extract_first(default=''))
        product['price_discount'] = self.clear_price(response.xpath(".//span[@class='product-old-price-inside']/text()").extract_first(default=''))
        product['currency'] ='RUB'
        yield product


    def clear_price(self, str):
        pattern = re.compile(r'(\s|\(.+\)|₽|\D)')
        return pattern.sub(u'', str)


















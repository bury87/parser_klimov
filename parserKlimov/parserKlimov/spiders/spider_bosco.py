# coding: utf8
import scrapy
import re
import json
import csv
import sys

from parserKlimov.items import BoscoItem

class BoscoSpider(scrapy.Spider):
    name = "bosco"
    pipeline = 'bosco'
    shop = {
		"url" : u"https://www.bosco.ru/",
		"title" : u"Интернет-магазин одежды – большой и удобный каталог модной одежды для женщин на сайте TrendsBrands.ru",
		"logo" : u"https://www.bosco.ru/bitrix/templates/bosco/images/logo-online.png"
	}


    def start_requests(self):
        urls = ['https://www.bosco.ru/shop/']
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)


    def parse(self, response):
        urls = response.xpath(".//div[@class='header-main-menu header-main-menu__big']/a/@href").extract()
        for url in urls:
            yield scrapy.Request(url=response.urljoin(url), callback=self.parse1)


    def parse1(self, response):
        urls1 = response.xpath(".//a[contains(@class,'vertical-left-menu-lnk')]/@href").extract()
        urls2 = response.xpath(".//div[@class='twins-cols-left']/ul[contains(@class,'vertical-left-menu')]/li/a/@href").extract()
        for url in urls1:
            yield scrapy.Request(url=response.urljoin(url), callback=self.parse2)
        for url in urls2:
            yield scrapy.Request(url=response.urljoin(url), callback=self.parse2)

    def parse2(self, response):
        urls = response.xpath(".//div[@class='parent-side-section']/ul[contains(@class,'vertical-left-menu')]/li/a/@href").extract()
        for url in urls:
            yield scrapy.Request(url=response.urljoin(url), callback=self.parse3)

    def parse3(self, response):
        urls = response.xpath(".//a[@data-type='perehod_na_kartochku_tovara' and contains(@class,'catalog-list-item-title')]/@href").extract()
        cat = response.xpath(".//div[@class='breadcrumbs']/a/text()").extract()

        cat.append(response.xpath(".//span[@class='last-breadcrumbs']/text()").extract_first(default=''))
        for url in urls:
            yield scrapy.Request(url=response.urljoin(url), callback=self.parse4, meta={'cat':cat})
        next_page = response.xpath(".//a[contains(@class,'pagination-by-step-right')]/@href").extract_first(
            default=None)
        if next_page is not None:
            yield scrapy.Request(url=response.urljoin(next_page), callback=self.parse3)

    def parse4(self, response):
        product = BoscoItem()
        product['url'] = response.url
        product['brand'] = response.xpath(
            ".//div[contains(@class,'shop-item-brand')]/a/text()").extract_first()
        product['cat'] = response.meta['cat']
        print product['cat']
        product['img'] = response.xpath(".//img[contains(@class,'detail-slider-preview-js')]/@src").extract()
        product['title'] = response.xpath(".//h1[contains(@class,'shop-item-title')]/text()").extract_first()
        product['price'] = self.clear_price(response.xpath(".//div[contains(@class,'shop-item-price cur-price-js')]/@content").extract_first(
            default=''))
        if product['price'] == '':
            product['price'] = response.xpath(".//meta[contains(@itemprop,'price')]/@content").extract_first(
            default='')
            product['price_discount'] = self.clear_price(response.xpath(
                ".//div[contains(@class,'shop-item-price-old')]/text()").extract_first(default=''))
        product['currency'] = 'RUB'
        yield product


    def clear_price(self, str):
        pattern = re.compile(r'(\s|\(.+\)|₽|\D)')
        return pattern.sub(u'', str)


















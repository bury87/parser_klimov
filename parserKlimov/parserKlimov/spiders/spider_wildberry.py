# coding: utf8
import scrapy
import re
import json
import csv
import sys

from parserKlimov.items import WildberryItem

class WildberrySpider(scrapy.Spider):
    name = "wildberry"
    pipeline = 'wildberry'
    shop = {
		"url" : u"https://www.wildberries.ru/",
		"title" : u"WildBerries.ru – Интернет-магазин модной одежды и обуви",
		"logo" : u"//images.wbstatic.net/logotip/klaksa2.png"
	}


    def start_requests(self):
        urls = ['https://www.wildberries.ru/']
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)


    def parse(self, response):
        urls = response.xpath(".//ul[@class='topmenus']/li[position()<29]/a/@href").extract()
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse1)


    def parse1(self, response):
        urls = response.xpath(".//li[contains(@class,'maincatalog-menu-item') and position()>1]/a/@href").extract()
        for url in urls:
                yield scrapy.Request(url=response.urljoin(url), callback=self.parse2)

    def parse2(self, response):
        urls = response.xpath(".//li[@class='selected hasnochild']/ul/li/a/@href").extract()
        for url in urls:
                yield scrapy.Request(url=response.urljoin(url), callback=self.parse3)


    def parse3(self, response):
        urls = response.xpath(".//div[@class='catalog_main_table']/div[@class='dtList']/a/@href").extract()
        cat = response.xpath(".//div[@class='breadcrumbs']/div/a/span/text()").extract()
        cat.append(response.xpath(".//div[@class='breadcrumbs']/div/span/text()").extract_first())
        for url in urls:
            yield scrapy.Request(url=response.urljoin(url), callback=self.parse4, meta={'cat':cat})
        next_page = response.xpath(".//ul[@class='list-pagination']/li[@class='next']/a/@href").extract_first(
            default=None)
        if next_page is not None:
            yield scrapy.Request(url=response.urljoin(next_page), callback=self.parse3)

    def parse4(self, response):
        product = WildberryItem()
        product['url'] = response.url
        product['cat'] = response.meta['cat']
        # print ','.join(product['cat']).encode('cp1251')
        product['img'] =  response.xpath(".//ul[@class='carousel']/li/a/@href").extract()
        product['title'] = response.xpath(".//h1[@itemprop='name']/text()").extract_first().strip()
        product['price'] = response.xpath(".//meta[@itemprop='price']/@content").extract_first(default='')
        product['brand'] = response.xpath(".//a[@id='brandBannerImgRef']/@title").extract_first(default='')
        product['price_discount'] = response.xpath(".//span/del/text()").extract_first(default='').replace(u'руб.','').replace(u' ','').replace(u',','.').replace(u'\xa0', u'').replace(u'\n', u'')
        product['currency'] ='RUB'
        yield product


















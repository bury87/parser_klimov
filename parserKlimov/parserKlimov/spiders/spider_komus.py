# coding: utf8
import scrapy
import re
import json
import csv
import sys

from parserKlimov.items import KomusItem

class KomusSpider(scrapy.Spider):
    name = "komus"
    pipeline = 'komus'
    # handle_httpstatus_list = [301]
    shop = {
		"url" : u"http://www.komus.ru/",
		"title" : u"Интернет-магазин товаров для дома и офиса Комус: купить канцтовары, бумагу, хозтовары, подарки и мебель",
		"logo" : u"https://www.komus.ru/medias/sys_master/root/h99/h16/8986638417950.jpg"
	}


    def start_requests(self):
        urls = ['https://www.komus.ru/katalog/kantstovary/c/3303/']
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        url = response.xpath(".//div[contains(@class,'b-topMenu__item')]/a[@class='js-topMenuItemLink']/@href").extract_first(default=None)
        yield scrapy.Request(url=response.urljoin(url), callback=self.parse_cat_link)

    def parse_cat_link(self, response):
        urls = response.xpath(".//a[@class='b-account__item--label']/@href").extract()
        if len(urls)>0:
            for url in urls:
                yield scrapy.Request(url=response.urljoin(url), callback=self.parse_cat_link)
        else:
            urls = response.xpath(".//a[@class='b-productList__item__descr--title']/@href").extract()
            for url in urls:
                yield scrapy.Request(url=response.urljoin(url), callback=self.parse_item)
            next_url = response.xpath(".//a[@class='b-pageNumber__item b-pageNumber__item--text']/@href").extract_first(default=None)
            if next_url is not None:
                yield scrapy.Request(url=response.urljoin(next_url), callback=self.parse_cat_link)

    def parse_items(self, response):
        urls = response.xpath(".//a[@class='b-productList__item__descr--title']/@href").extract()
        for url in urls:
            yield scrapy.Request(url=response.urljoin(url), callback=self.parse_item)
        next_url = response.xpath(".//a[@class='b-pageNumber__item b-pageNumber__item--text']/@href").extract_first(
            default=None)
        if next_url is not None:
            yield scrapy.Request(url=response.urljoin(next_url), callback=self.parse_item)



    def parse_item(self, response):
        product = KomusItem()
        product['url'] = response.url
        product['brand'] = response.url

        product['cat'] = response.xpath(".//li[@class='b-breadcrumbs__list__item ']/a/span/text()").extract()
        # print ','.join(product['cat']).encode('cp1251')
        product['img'] = response.xpath(
            ".//div[@id='imagesList']/a/@data-image").extract()
        product['title'] = response.xpath(
            ".//h1[contains(@class,'b-productName')]/text()").extract_first()
        product['price_discount'] = response.xpath(".//span[@class='b-productParams__price--old']/text()").extract_first(
            default=None)
        if product['price_discount'] is not None:
            product['price_discount']=product['price_discount'].replace(u'р.','').replace(u' ','').replace(u',','.').replace(u'\xa0', u'').replace(u'\n', u'')
        if product['price_discount'] is None:
            product['price'] = response.xpath(
                ".//span[@class='b-productParams__price--currant']/span/text()").extract_first(default="").replace(u'р.','').replace(u' ','').replace(u',','.').replace(u'\xa0', u'').replace(u'\n', u'')
            product['price_discount'] = ''
        else:
            product['price'] = response.xpath(
                ".//span[@class='b-productParams__price--currant']/span/text()").extract_first(default="").replace(u'р.','').replace(u' ','').replace(u',','.').replace(u'\xa0', u'').replace(u'\n', u'')
        product['currency'] = 'RUB'
        yield product



















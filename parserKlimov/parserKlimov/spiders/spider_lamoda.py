# coding: utf8
import scrapy
import re
import json
import csv
import sys

from parserKlimov.items import LamodaItem

class LamodaSpider(scrapy.Spider):
    name = "lamoda"
    pipeline = 'lamoda'
    shop = {
		"url" : u"http://www.lamoda.ru/",
		"title" : u"Купить мужскую одежду и обувь в интернет магазине Lamoda.ru",
		"logo" : u"Lamoda"
	}


    def start_requests(self):
        urls = ['http://www.lamoda.ru/c/4153/default-women/']
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse3)


    def parse(self, response):
        urls = response.xpath(".//ul[@class='catalog-navigation show']/li[@class='catalog-navigation__item']/a/@href").extract()
        for url in urls:
            yield scrapy.Request(url=response.urljoin(url), callback=self.parse1)


    def parse1(self, response):
        urls = response.xpath(".//ul[@class='catalog-navigation show']/li[position()=1]/ul/li/a/@href").extract()
        if len(urls)>1:
            print 'parse1'
            for url in urls:
                yield scrapy.Request(url=response.urljoin(url), callback=self.parse2)
        else:
            self.parse3(response)

    def parse2(self, response):
        urls = response.xpath(".//ul[@class='catalog-navigation show']/li/ul/li[position()=1]/ul/li/a/@href").extract()
        if len(urls)>1:
            print 'parse2'
            for url in urls:
                yield scrapy.Request(url=response.urljoin(url), callback=self.parse2)
        else:
            self.parse3(response)


    def parse3(self, response):
        urls = response.xpath(".//div[@class='products-catalog__list']/div[@class='products-list-item']/a/@href").extract()
        print response.url
        for url in urls:
            yield scrapy.Request(url=response.urljoin(url), callback=self.parse4)
        pages = response.xpath(".//div[contains(@class,'paginator')]/@data-pages").extract_first(default=0)
        print pages
        if int(pages) >1:
            for page in range(2,int(pages)):
                url = str(response.url)+'?page='+str(page)
                yield scrapy.Request(url=url, callback=self.parse3pag)


    def parse3pag(self, response):
        urls = response.xpath(
            ".//div[@class='products-catalog__list']/div[@class='products-list-item']/a/@href").extract()
        print response.url
        for url in urls:
            yield scrapy.Request(url=response.urljoin(url), callback=self.parse4)




    def parse4(self, response):
        product = LamodaItem()
        product['url'] = response.url
        product['cat'] = response.xpath(".//div[contains(@class,'breadcrumbs')]/span/a/span/text()").extract()
        # print ','.join(product['cat']).encode('cp1251')
        product['img'] =  response.xpath(".//div[contains(@class,'showcase__slide showcase__slide_image')]/@data-resource").extract()
        product['title'] = response.xpath(".//h1[contains(@class,'heading_m ii-product__title')]/text()").extract_first()
        product['brand'] = response.xpath(".//a[contains(@class,'ii-product__brand-image lazy')]/@title)").extract_first(default='')
        product['price'] = response.xpath(".//div[contains(@class,'ii-product__price')]/@data-original").extract_first(default=None)
        if product['price'] is None:
            product['price'] = response.xpath(".//div[contains(@class,'ii-product__price')]/@data-current").extract_first(default=0)
            product['price_discount'] = ''
        else:
            product['price_discount'] = response.xpath(".//div[contains(@class,'ii-product__price')]/@data-current").extract_first(default=0)
        product['currency'] ='RUB'
        yield product


















# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
from json import  dumps
import codecs
from codecs import getwriter
from sys import stdout
import logging
import json

class ParserklimovPipeline(object):
    def __init__(self):
        self.category = {}
        self.shop = {}
        self.product_list = []

    def put_category(self, cat, lst, i=0):
        if i <len(lst):
            if lst[i] not in cat:
                cat.update({lst[i]:{'category':{'title':lst[i]}, 'product':[]}})
            cat[lst[i]].update(self.put_category(cat[lst[i]], lst, i+1))
        return cat

    def add_product(self, cat, lst, product):
        if len(lst) == 1:
            cat[lst[0]]['product'].append(product)
        else:
            if len(lst) > 0:
                l = lst.pop(0)
                cat[l].update(self.add_product(cat[l], lst, product))
        return cat

    def open_spider(self, spider):
        self.shop = spider.shop

    def process_item(self, product, spider):
        if spider.pipeline in ['lamoda','trendsbrands','respublica','ozon','komus','hoff','wildberry', 'bosco','dressone','kalashnikovy','roomchik']:
            category2 = product['cat']
            category1 = product['cat']
            # print category1
            self.category = self.put_category(self.category, category2,0)
            # print self.category
            if spider.pipeline in ['dressone','trendsbrands',]:
                product_prepare = {'images': product['img'], 'title': product['title'],
                                   'discount': product['price_discount'], 'price': product['price'],
                                   'currency': product['currency'], 'brand': product['brand'], 'description': product['description']}
            else:
                product_prepare = {'images': product['img'],'title': product['title'], 'discount': product['price_discount'], 'price': product['price'], 'currency': product['currency'],'brand': product['brand']}
            # print '44444'
            # print product1['cat']

            self.category = self.add_product(self.category, category1, product_prepare)


        return product

    def prepare_dict(self, dct):
        lst = []
        if 'category' in dct and 'product' in dct:
            lst.append({'category':dct['category'],'product':dct['product']})
        for d in dct:
            if d not in ['category','product']:
                lst.append(self.prepare_dict(dct[d]))
        return lst


    def close_spider(self, spider):

        print self.prepare_dict(self.category)
        ouput_dict = {'shop': self.shop,'catalog': self.prepare_dict(self.category)}
        json_string = dumps(dict(ouput_dict), ensure_ascii=False, encoding="utf-8")
        # print json_string
        try:
            file_json=codecs.open('result/'+spider.name+'.json', 'w','utf-8')
        except Exception:
            file_json = codecs.open(spider.name+'.json', 'w', 'utf-8')
        file_json.write(json_string)
        file_json.close()


class ItemPreparePineline(object):
    # TODO pineline for clear data
    def process_item(self, product, spider):

        for item in product:
            if type(product[item]) is str:
                product[item] = product[item].strip()

        return product


